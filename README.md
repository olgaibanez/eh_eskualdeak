# Egunean Behin programazio leihaketa


[![](https://img.youtube.com/vi/VgYAIr34FVc/hqdefault.jpg)](https://www.youtube.com/watch?v=VgYAIr34FVc "Egunean Behin lehiaketaren aurkezpena")


## Leihaketaren oinarriak

CodeSyntax enpresak, Informatika Fakultateak eta Gipuzkoako Campuseko Errektoreordetzak elkarlanean sortutako lehiaketa. 
Lehiaketak galde-erantzun sistematiko edo automatikoak sortzea zuen helburu. [Deialdia](https://www.ehu.eus/es/web/informatika-fakultatea/egunean-behin) eta lehiaketaren [oinarriak](https://www.ehu.eus/ehusfera/ehukultura/files/2020/01/Egunean-Behin-Galdera-Lehiaketa-1.0.pdf) EHUren webgunean eskuragarri daude. 



